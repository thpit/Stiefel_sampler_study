# Stiefel sampler study

An example implementation of a Stiefel manifold sampler, using O(n^2) standard normal 
drawings per realization \in \Real^{n \times n}. This is an alternative approach to,
and was motivated by, [1].

# License
Main file (StiefelManifoldSampler.py) is MIT licensed.

# Screenshots
![Output for d=3](img_out/notebook_graphics_d3_N1000_3rd_row.png)

# References
[1] https://github.com/RSNirwan/HouseholderBPCA and related publications

