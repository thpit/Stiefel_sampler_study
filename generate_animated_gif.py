# This is for testing and visualization only.

# python 3 code

import numpy as np
from numpy.random import default_rng
rng = default_rng()

from StiefelManifoldSampler import StiefelManifoldSampler 


d=3
smf1 = StiefelManifoldSampler(d, d)

N = 500
print("N:", N)
print("d:", d)

res = np.zeros((N, d, d))
for cyc in range(N):
    res[cyc,:,:] = smf1.sample()








# Adapted from https://nathankjer.com/animated-3d-plots/:

from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt


# Same as in notebook, but animated:
fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(projection="3d")
 
# Show, of the result matrices, only the last column vector as point in the plot
ax.scatter3D(res[:,0,2], res[:,1,2], res[:,2,2], color = "green")
plt.title("3rd column of Stiefel realizations")

ax.set_xlabel('x_0')
ax.set_ylabel('x_1')
ax.set_zlabel('x_2')

def update(i, fig, ax):
    ax.view_init(elev=20., azim=i)
    return fig, ax
 
anim = FuncAnimation(fig, update, frames=np.arange(0, 360, 2), repeat=True, fargs=(fig, ax))
anim.save('sample_points_animated.gif', dpi=80, writer='imagemagick', fps=24)

