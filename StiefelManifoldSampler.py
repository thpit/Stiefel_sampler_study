# (C) 2021 Thomas Pitschel
# MIT license.

# python 3 code

import numpy as np

from numpy.random import default_rng
rng = default_rng()

class StiefelManifoldSampler:
    """
    For given n,k, the Stiefel manifold is the set of all matrices 
    $\Real^{n \times k}$ such that all columns are orthogonal
    and have unit Euclidean length. Uniformly sampling
    from this manifold means to let each column vector
    have uniform distribution on the sphere in $\Real^n$,
    and to let each column vector be independently 
    distributed from the others - within the above said
    constraint.
    
    Sampling points from the manifold in accordance with
    the uniform distribution has applications in Bayesian 
    estimation, see [1] for example.
    
    The implementation here for a sampler tries to improve
    upon the mechanism of generating the samples, compared 
    with the one used in [1], with ultimate goal of speedup.
    
    The approach here requires, for each result matrix sample,
    n*n std-normal drawings. (The d.o.f. while realizing a 
    matrix from the manifold is ... . E.g. in dimension 2 there
    suffices one parameter, the rotation angle, to determine
    the matrix, if determinant is fixed to +1. In dimension 3,
    three angles suffice (see e.g. Euler angles), and so on.)
    
    An alternative implementation might use n+(n-1)+(n-2)+..+1
    (i.e. still O(n^2)) standard normals, using an embedding
    matrix A_i which is determined, in each iteration, by 
    res[:,0:i].T A_i = 0. [Using such embedding matrix 
    may be equivalent to the approach in [1]; the remaining 
    point then is how to determine a suitable such matrix 
    in the most efficient way.]
    
    Further speedup possibility: do rather use a specialized 
    "sphere sampler", rather than standard normal.
    
    References:
    
    [1] https://proceedings.mlr.press/v97/nirwan19a.html
    """
    
    def __init__(self, n, k, do_debug=False):
        self.n = n
        self.k = k
        self.param1 = 3 
            # This parameter controls if the sampling will 
            # include more rejections (parameter smaller)
            # but then deliver numerically better results,
            # or include fewer rejections (parameter larger,
            # up to infinity) but then gets tends to encounter
            # numerical issues (division by almost zero etc)
            # for some samples. 
            # Value can be left unchanged for most of the 
            # application cases. The parameter is emlpoyed
            # in a way such that varying dimensionality is
            # automatically taking into account.
        self.do_debug = do_debug    # whether to print internal msgs
        
    @staticmethod
    def sample_from_mv_gaussian(d):
        """
        Sample from a d-dimensional Gaussian distribution with
        zero mean and covariance equalling identity matrix.
        """
        return rng.standard_normal(d)
        
    def sample_from_sphere(self, d):
        while True:
            cand = StiefelManifoldSampler.sample_from_mv_gaussian(d)
            cand_length_squ = np.sum(cand*cand)
            if cand_length_squ > np.exp(- self.param1 * d):
                break
            if self.do_debug: print("I: Rejection case encountered. unit vector.")
        cand_nrm = cand / np.sqrt(cand_length_squ)
        return cand_nrm
        
    def sample(self):
        # Idea: 
        # - successively add a new vector v_i to the result matrix,
        #   working in successively lower dimension while proceeding
        #   from i=1 to k.
        
        res = np.zeros((self.n, self.k))
        for i in range(self.k):
            while True:
                v = self.sample_from_sphere(self.n)
                if i==0: break
                # Project v such that u = P_i(v) is orthogonal to all 
                # vectors res[:,i'], i' = 0...i-1:
                u = v - res[:,0:i] @ (res[:,0:i].T @ v)
                # The projected vector, even though of a length with 
                # "unknown" (more exact: irregular) distribution, is still
                # directionally uniform in the lower-dim space, i.e. in 
                # the S^{n-i}-isomorphic subspace.
                u_length_squ = np.sum(u*u)
                if u_length_squ > np.exp(- self.param1 * (self.n-i)):
                    break
                if self.do_debug: print("I: Rejection case encountered. main loop.")
            if i==0:
                res[:,i] = v
                continue
            u_nrm = u / np.sqrt(u_length_squ)
            # This vector is uniform on the S^{n-i}-isomorphic subspace
            # of S^n which is orthogonal to all columns in res[:,0:(i-1)].
            # The last vector to be set, i.e. for i=n-1, essentially only
            # has the d.o.f. of its flipping sign.
            res[:,i] = u_nrm
        return res
        
    # For testing only. Makes sense only for k=n.
    @staticmethod
    def deviation_from_unity_matrix(mat1):
        """
        L_2 distance.
        """
        n = mat1.shape[0]
        buf1 = mat1-np.eye(n)
        return np.sqrt(np.sum(buf1*buf1))/n

if __name__ == "__main__":
    
    # All examples use k = n = d.
    
    d=3
    smf1 = StiefelManifoldSampler(d, d)
    
    N = 100
    print("N:", N)
    print("d:", d)
    
    for cyc in range(N):
        print("---------------------------")
        mat1 = smf1.sample()
        print(mat1)
        print("Check:")
        buf1 = mat1.T @ mat1
        if d <= 16:
            print(buf1)
        print("L_2 dist: ", StiefelManifoldSampler.deviation_from_unity_matrix(buf1))
        print()
        
    
    
    
        